**Microsoft azure Container Service**

I’m excited to announce the general availability of the Microsoft Azure Container Service; the simplest, most open and flexible way to run your container applications in the cloud.

But why a container service? Lately it seems like you can’t have a discussion about cloud computing without also talking about the benefit of container ecosystems for development agility and portability. Organizations are already experimenting with container technology in an effort to understand what they mean for applications in the cloud and on-premises, and how to best use them for their specific development and IT operations scenarios.

However, as organizations adopt containers and look to scale them in production, they discover that deploying and operating containerized application workloads is a non-trivial exercise. The complexity of tracking and managing high-density containers at scale grows at an exponential rate, making traditional, hands-on management approaches ineffective.

[Automize](https://automize.dk/kompetencer/microsoft-azure-cloud/), microsoft azure container service addresses these challenges by providing simplified configurations of proven open source container orchestration technology, optimized to run in the cloud. With just a few clicks you can deploy your container-based applications on a framework designed to help manage the complexity of containers deployed at scale, in production.

We built Microsoft Azure Container Service to be the fastest way to get the benefits of running containerized applications, using your choice of open source technology, tools and skills and with the support of a thriving community and ecosystem.

With Docker image support and our commitment to open source software in the orchestration layer, your application is fully portable across any cloud and on-premises. Unlike other container services, the Microsoft Azure Container Service is built on 100% open source software to maximize portability of workloads and offers a choice among popular orchestration engines.

